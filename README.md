# purchase-simcard

Java development challenge for purchasing prepaid data for SIM card by getting a voucher code from 3rd party.

##1. What are done?
- Manage network Suppliers: Viettel, Mobi, Vietnamobile
- Manage kinds of phone card: 10.000, 20.000, 50.000, 100.000, 200.000, 500.000
- Purchase a phone card:
  + validate invoice request: banking account, selected card, network supplier, available balances and manage exception
  + public 3rd API to generate dummy voucher code, cancel voucher when the process has an error
  + register data: calculate available balance for banking account, create transaction history, create purchase card record

##2. What are remaining?
- Cannot send SMS mobile by java code: cannot get trial phone number from https://www.twilio.com/
- Network traffic does not handel.
- Checking voucher code by customer is not supported.

##3. How to check run purchase-simcard application?
build and command line: java -jar target/purchase-simcard-1.0.0.jar

##4. Dummy data in H2 DB
- H2 DB info:
  + URL: http://localhost:8080/phone-card/rest/h2-console
  + JDBC URL: jdbc:h2:mem:phone_card_db
  + username: sa, password: none
- Table BANK:

|id|address|code|name|
|:-------:|:------:|:------:|:------:|
|1|Australia|NAB|National Australia Bank|

- Table BANK_ACCOUNT:

|id|account_name|authorised_signature|available_balances|card_valid_period|creation_date|email|phone_number|pin_number|visa_card_number|bank_id|
|:------:|:------:|:------|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
|1|hoang.dong|111|10000000.00|202512|2021-04-18|davidtroc0903@gmail.com|0982291607|123456|0123456789|1|

- Table CARD_SUPPLIER:

|id|code|name|
|:-------:|:------:|:------:|
|1|VTEL|Viettel|
|2|MOBI|Mobiphone|
|3|VNMB|Vietnamobile|

- Table CARD_KIND:

|code|description|price|supplier_id|
|:-------:|:------:|:------:|:------:|
|0010|10,000 VND|10000.00|1|
|0020|20,000 VND|20000.00|1|
|0050|50,000 VND|50000.00|1|
|0100|100,000 VND|100000.00|1|
|0200|200,000 VND|200000.00|1|
|0500|500,000 VND|500000.00|1|
|...|...|...|2|
|...|...|...|3|

##5. Postman collection attachment.

##### Rest URL: http://localhost:8080/phone-card/rest

##### Postman path: `src\main\resources\postman\purchase_phone_card.postman_collection`

##### List of APIs:
- get-all-suppliers: get all network suppliers
- get-supplier-by-code: get network supplier by code
- get-all-kinds: get all phone card kinds
- get-kind-by-supplier: get all card kinds by supplier code
- thirdparty-register-viettel-voucher: generate voucher from Viettel network company
- thirdparty-register-mobiphone-voucher: generate voucher from Mobiphone network company
- thirdparty-register-vietnamobil-voucher: generate voucher from Vietnamobi network company
- register-card: purchase phone card

###### Remark: the data of register-card rest service:

```
{
  "kindCode": "0020",
  "supplierCode": "VTEL",
  "phoneNumber": "84982291607",
  "accountName": "hoang.dong",
  "visaCardNumber": "0123456789",
  "validPeriod": "202512",
  "authorisedSignature": "111",
  "pinNumber": "123456"
}
```

- phone number must be follow the pattern: `(\\+84|84|0)+(9|1[0-9])+([0-9]{8})\\b`
- all the info of banking account must be valid like the data from table `BANK_ACCOUNT`
- valid period must be greater than current period
