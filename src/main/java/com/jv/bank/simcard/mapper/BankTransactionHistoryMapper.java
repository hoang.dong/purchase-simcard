package com.jv.bank.simcard.mapper;

import com.jv.bank.simcard.abstracts.AbstractMapper;
import com.jv.bank.simcard.entity.BankTransactionHistoryEntity;
import com.jv.bank.simcard.model.BankTransactionHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Component
public class BankTransactionHistoryMapper extends AbstractMapper<BankTransactionHistory, BankTransactionHistoryEntity> {

    @Autowired
    BankAccountMapper accountMapper;

    @Override
    public BankTransactionHistory toModel(BankTransactionHistoryEntity entity) {
        if (entity == null) {
            return null;
        }
        BankTransactionHistory dto = new BankTransactionHistory();
        dto.setId(entity.getId());
        dto.setAccount(accountMapper.toModel(entity.getAccount()));
        dto.setTransactionType(entity.getTransactionType());
        dto.setDescription(entity.getDescription());
        dto.setCurrentBalance(entity.getCurrentBalance());
        dto.setTransactionAmount(entity.getTransactionAmount());
        dto.setNewBalance(entity.getNewBalance());
        dto.setTransactionDate(entity.getTransactionDate());
        return dto;
    }

    @Override
    public BankTransactionHistoryEntity toEntity(BankTransactionHistory dto) {
        if (dto == null) {
            return null;
        }
        BankTransactionHistoryEntity entity = new BankTransactionHistoryEntity();
        entity.setId(dto.getId());
        entity.setAccount(accountMapper.toEntity(dto.getAccount()));
        entity.setTransactionType(dto.getTransactionType());
        entity.setDescription(dto.getDescription());
        entity.setCurrentBalance(dto.getCurrentBalance());
        entity.setTransactionAmount(dto.getTransactionAmount());
        entity.setNewBalance(dto.getNewBalance());
        entity.setTransactionDate(dto.getTransactionDate());
        return entity;
    }
}
