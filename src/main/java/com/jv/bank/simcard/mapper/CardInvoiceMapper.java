package com.jv.bank.simcard.mapper;

import com.jv.bank.simcard.abstracts.AbstractMapper;
import com.jv.bank.simcard.entity.CardInvoiceEntity;
import com.jv.bank.simcard.model.CardInvoice;
import com.jv.bank.simcard.model.InvoiceRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Component
public class CardInvoiceMapper extends AbstractMapper<CardInvoice, CardInvoiceEntity> {

    @Autowired
    ModelMapper modelMapper;

    @Override
    public CardInvoice toModel(CardInvoiceEntity entity) {
        return modelMapper.map(entity, CardInvoice.class);
    }

    @Override
    public CardInvoiceEntity toEntity(CardInvoice user) {
        return this.modelMapper.map(user, CardInvoiceEntity.class);
    }

    public CardInvoice toInvoiceDto(InvoiceRequest invoiceRequestDto) {
        CardInvoice invoiceDto = new CardInvoice();
        invoiceDto.setKindCode(invoiceRequestDto.getKindCode());
        invoiceDto.setSupplierCode(invoiceRequestDto.getSupplierCode());
        invoiceDto.setPhoneNumber(invoiceRequestDto.getPhoneNumber());
        invoiceDto.setBankAccountName(invoiceRequestDto.getAccountName());
        invoiceDto.setBankAccountNumber(invoiceRequestDto.getVisaCardNumber());
        invoiceDto.setBankValidPeriod(invoiceRequestDto.getValidPeriod());
        invoiceDto.setBankAuthorisedSignature(invoiceRequestDto.getAuthorisedSignature());
        invoiceDto.setKindCode(invoiceRequestDto.getKindCode());
        return invoiceDto;
    }
}
