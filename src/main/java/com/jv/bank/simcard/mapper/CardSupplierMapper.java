package com.jv.bank.simcard.mapper;

import com.jv.bank.simcard.abstracts.AbstractMapper;
import com.jv.bank.simcard.entity.CardSupplierEntity;
import com.jv.bank.simcard.model.CardSupplier;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Component
public class CardSupplierMapper extends AbstractMapper<CardSupplier, CardSupplierEntity> {

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public CardSupplier toModel(CardSupplierEntity entity) {
        return this.modelMapper.map(entity, CardSupplier.class);
    }

    @Override
    public CardSupplierEntity toEntity(CardSupplier model) {
        return this.modelMapper.map(model, CardSupplierEntity.class);
    }
}
