package com.jv.bank.simcard.mapper;

import com.jv.bank.simcard.abstracts.AbstractMapper;
import com.jv.bank.simcard.entity.CardKindEntity;
import com.jv.bank.simcard.entity.CardKindPK;
import com.jv.bank.simcard.model.CardKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Component
public class CardKindMapper extends AbstractMapper<CardKind, CardKindEntity> {

    @Autowired
    CardSupplierMapper supplierMapper;

    @Override
    public CardKind toModel(CardKindEntity entity) {
        if (entity == null) {
            return null;
        }
        CardKind dto = new CardKind();
        dto.setCode(entity.getId().getCode());
        dto.setSupplier(supplierMapper.toModel(entity.getId().getSupplier()));
        dto.setDescription(entity.getDescription());
        dto.setPrice(entity.getPrice());
        return dto;
    }

    @Override
    public CardKindEntity toEntity(CardKind dto) {
        if (dto == null) {
            return null;
        }
        CardKindEntity entity = new CardKindEntity();
        entity.setId(new CardKindPK(dto.getCode(), supplierMapper.toEntity(dto.getSupplier())));
        entity.setDescription(dto.getDescription());
        entity.setPrice(dto.getPrice());
        return entity;
    }
}
