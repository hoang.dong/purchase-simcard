package com.jv.bank.simcard.mapper;

import com.jv.bank.simcard.abstracts.AbstractMapper;
import com.jv.bank.simcard.entity.BankEntity;
import com.jv.bank.simcard.model.Bank;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Component
public class BankMapper extends AbstractMapper<Bank, BankEntity> {

    @Autowired
    ModelMapper modelMapper;

    @Override
    public Bank toModel(BankEntity entity) {
        return modelMapper.map(entity, Bank.class);
    }

    @Override
    public BankEntity toEntity(Bank dto) {
        return this.modelMapper.map(dto, BankEntity.class);
    }
}
