package com.jv.bank.simcard.mapper;

import com.jv.bank.simcard.abstracts.AbstractMapper;
import com.jv.bank.simcard.entity.BankAccountEntity;
import com.jv.bank.simcard.model.BankAccount;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Component
public class BankAccountMapper extends AbstractMapper<BankAccount, BankAccountEntity> {

    @Autowired
    ModelMapper modelMapper;

    @Override
    public BankAccount toModel(BankAccountEntity entity) {
        return modelMapper.map(entity, BankAccount.class);
    }

    @Override
    public BankAccountEntity toEntity(BankAccount user) {
        return this.modelMapper.map(user, BankAccountEntity.class);
    }
}