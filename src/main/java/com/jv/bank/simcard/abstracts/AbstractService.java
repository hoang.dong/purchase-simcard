package com.jv.bank.simcard.abstracts;

import com.jv.bank.simcard.exception.NotFoundException;

import java.util.List;

/**
 * interface with signatures method for DAO
 *
 * @author hoang.dong
 */
public interface AbstractService<M, T> {

    M findById(T id) throws NotFoundException;

    T create(M model);

    void update(M model);

    void remove(T id);

    List<M> findAll();
}
