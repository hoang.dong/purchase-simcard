package com.jv.bank.simcard.abstracts;

import com.jv.bank.simcard.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author hoang.dong
 */
@Component
public abstract class AbstractServiceImpl<
        T, // data type of identifier (id, pk)
        Entity extends AbstractEntity<T>, //  entity class
        Model, // model class
        Mapper extends AbstractMapper<Model, Entity>, // mapper abstract
        Repository extends JpaRepository<Entity, T> // repository
        >
        implements AbstractService<Model, T> {

    @Autowired
    protected Mapper mapper;
    @Autowired
    protected Repository repository;

    @Override
    public Model findById(T id) throws NotFoundException {
        Optional<Entity> optional = repository.findById(id);
        if (optional.isPresent()) {
            return this.mapper.toModel(optional.get());
        }
        throw new NotFoundException(this.getClass(), "id", id.toString());
    }

    @Override
    public T create(Model model) {
        Entity entity = mapper.toEntity(model);
        repository.save(entity);
        return entity.getId();
    }

    @Override
    public void update(Model model) {
        repository.save(mapper.toEntity(model));
    }

    @Override
    public void remove(T id) {
        Optional<Entity> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.delete(optional.get());
        }
    }

    @Override
    public List<Model> findAll() {
        return mapper.toModels(repository.findAll());
    }
}
