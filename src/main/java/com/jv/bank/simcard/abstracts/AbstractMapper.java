package com.jv.bank.simcard.abstracts;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * abstract class for convert data from model to entity and opposite
 *
 * @author hoang.dong
 */
public abstract class AbstractMapper<Model, Entity> {

    public abstract Model toModel(Entity entity);

    public abstract Entity toEntity(Model model);

    public List<Model> toModels(List<Entity> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return new ArrayList<>();
        }
        return entities.stream().map(entity -> toModel(entity)).collect(Collectors.toList());
    }

    public List<Entity> toEntities(List<Model> models) {
        if (CollectionUtils.isEmpty(models)) {
            return new ArrayList<>();
        }
        return models.stream().map(model -> toEntity(model)).collect(Collectors.toList());
    }
}