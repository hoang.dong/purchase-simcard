package com.jv.bank.simcard.abstracts;

import java.io.Serializable;

/**
 * @author hoang.dong
 */
public abstract class AbstractEntity<T> implements Serializable {
    public abstract T getId();
}
