package com.jv.bank.simcard.service;

import com.jv.bank.simcard.abstracts.AbstractService;
import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.model.CardSupplier;

/**
 * @author hoang.dong
 */
public interface CardSupplierService extends AbstractService<CardSupplier, Integer> {

    CardSupplier getByCode(String code) throws NotFoundException;
}
