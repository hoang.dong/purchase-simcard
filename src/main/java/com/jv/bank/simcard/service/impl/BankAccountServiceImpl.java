package com.jv.bank.simcard.service.impl;

import com.jv.bank.simcard.abstracts.AbstractServiceImpl;
import com.jv.bank.simcard.entity.BankAccountEntity;
import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.mapper.BankAccountMapper;
import com.jv.bank.simcard.model.BankAccount;
import com.jv.bank.simcard.repository.BankAccountRepository;
import com.jv.bank.simcard.service.BankAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class BankAccountServiceImpl extends AbstractServiceImpl<
        Integer,
        BankAccountEntity,
        BankAccount,
        BankAccountMapper,
        BankAccountRepository
        >
        implements BankAccountService {

    @Override
    public BankAccount getByVisaCardNumber(String cardNumber) throws NotFoundException {
        BankAccountEntity entity = repository.getByVisaCardNumber(cardNumber);
        if (entity == null) {
            log.warn("Cannot find banking account by cardNumber({})", cardNumber);
            throw new NotFoundException(BankAccountService.class.getName(), "email", cardNumber);
        }
        return mapper.toModel(entity);
    }
}
