package com.jv.bank.simcard.service.impl;

import com.jv.bank.simcard.abstracts.AbstractServiceImpl;
import com.jv.bank.simcard.entity.BankTransactionHistoryEntity;
import com.jv.bank.simcard.mapper.BankTransactionHistoryMapper;
import com.jv.bank.simcard.model.BankTransactionHistory;
import com.jv.bank.simcard.repository.BankTransactionHistoryRepository;
import com.jv.bank.simcard.service.BankTransactionHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class BankTransactionHistoryServiceImpl extends AbstractServiceImpl<
        BigInteger,
        BankTransactionHistoryEntity,
        BankTransactionHistory,
        BankTransactionHistoryMapper,
        BankTransactionHistoryRepository
        >
        implements BankTransactionHistoryService {
}