package com.jv.bank.simcard.service;

/**
 * @author hoang.dong
 */
public interface VoucherGenerateService {

    String registerVoucher(String supplierCode, String kindCode);

    boolean cancelVoucher(String voucher);
}
