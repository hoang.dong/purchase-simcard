package com.jv.bank.simcard.service.impl;

import com.jv.bank.simcard.abstracts.AbstractServiceImpl;
import com.jv.bank.simcard.entity.CardSupplierEntity;
import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.mapper.CardSupplierMapper;
import com.jv.bank.simcard.service.CardSupplierService;
import com.jv.bank.simcard.model.CardSupplier;
import com.jv.bank.simcard.repository.CardSupplierRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class CardSupplierServiceImpl
        extends AbstractServiceImpl<
        Integer,
        CardSupplierEntity,
        CardSupplier,
        CardSupplierMapper,
        CardSupplierRepository
        >
        implements CardSupplierService {

    @Override
    public CardSupplier getByCode(String code) throws NotFoundException {
        CardSupplierEntity entity = repository.getByCode(code);
        if (entity == null) {
            log.warn("Cannot find supplier by code ({})", code);
            throw new NotFoundException(CardSupplierService.class.getName(), "code", code);
        }
        return mapper.toModel(entity);
    }
}
