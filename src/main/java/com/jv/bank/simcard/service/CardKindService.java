package com.jv.bank.simcard.service;

import com.jv.bank.simcard.abstracts.AbstractService;
import com.jv.bank.simcard.entity.CardKindPK;
import com.jv.bank.simcard.exception.ComponentException;
import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.model.CardKind;

import java.util.List;

/**
 * @author hoang.dong
 */
public interface CardKindService extends AbstractService<CardKind, CardKindPK> {

    CardKind findByIdentifier(String kindCode, String supplierCode) throws NotFoundException;

    List<CardKind> getBySupplierCode(String supplierCode) throws ComponentException;
}
