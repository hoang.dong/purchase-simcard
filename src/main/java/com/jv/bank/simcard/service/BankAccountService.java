package com.jv.bank.simcard.service;

import com.jv.bank.simcard.abstracts.AbstractService;
import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.model.BankAccount;

/**
 * @author hoang.dong
 */
public interface BankAccountService extends AbstractService<BankAccount, Integer> {

    BankAccount getByVisaCardNumber(String cardNumber) throws NotFoundException;
}
