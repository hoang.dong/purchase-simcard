package com.jv.bank.simcard.service.impl;

import com.jv.bank.simcard.abstracts.AbstractServiceImpl;
import com.jv.bank.simcard.entity.CardKindEntity;
import com.jv.bank.simcard.entity.CardKindPK;
import com.jv.bank.simcard.entity.CardSupplierEntity;
import com.jv.bank.simcard.exception.ComponentException;
import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.exception.ValidateException;
import com.jv.bank.simcard.mapper.CardKindMapper;
import com.jv.bank.simcard.model.CardKind;
import com.jv.bank.simcard.repository.CardKindRepository;
import com.jv.bank.simcard.repository.CardSupplierRepository;
import com.jv.bank.simcard.service.CardKindService;
import com.jv.bank.simcard.service.CardSupplierService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class CardKindServiceImpl
        extends AbstractServiceImpl<
        CardKindPK,
        CardKindEntity,
        CardKind,
        CardKindMapper,
        CardKindRepository
        >
        implements CardKindService {

    @Autowired
    CardSupplierRepository supplierRepository;

    @Override
    public CardKind findByIdentifier(String kindCode, String supplierCode) throws NotFoundException {
        CardSupplierEntity supplier = supplierRepository.getByCode(supplierCode);
        if (supplier == null) {
            log.warn("supplier({}) is not existed", supplierCode);
            throw new NotFoundException(CardSupplierService.class.getName(), "supplierCode", supplierCode);
        }

        Optional<CardKindEntity> optional = repository.findById(new CardKindPK(kindCode, supplier));
        if (!optional.isPresent()) {
            log.warn("Cannot find Card kind by kindCode({}) and supplier({})", kindCode, supplierCode);
            throw new NotFoundException(CardKindService.class.getName(), "kindCode", kindCode);
        }
        return mapper.toModel(optional.get());
    }

    @Override
    public List<CardKind> getBySupplierCode(String supplierCode) throws ComponentException {
        if (StringUtils.isBlank(supplierCode)) {
            log.warn("supplier code can not be blank");
            throw new ValidateException("supplier code can not be blank");
        }
        return mapper.toModels(repository.getByIdSupplierCode(supplierCode));
    }
}