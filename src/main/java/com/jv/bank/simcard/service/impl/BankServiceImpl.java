package com.jv.bank.simcard.service.impl;

import com.jv.bank.simcard.abstracts.AbstractServiceImpl;
import com.jv.bank.simcard.entity.BankEntity;
import com.jv.bank.simcard.mapper.BankMapper;
import com.jv.bank.simcard.model.Bank;
import com.jv.bank.simcard.repository.BankRepository;
import com.jv.bank.simcard.service.BankService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class BankServiceImpl extends AbstractServiceImpl<
        Integer,
        BankEntity,
        Bank,
        BankMapper,
        BankRepository
        >
        implements BankService {
}
