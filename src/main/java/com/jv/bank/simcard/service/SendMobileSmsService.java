package com.jv.bank.simcard.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 * @author hoang.dong
 */
public class SendMobileSmsService {

    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC98f03ecb5284e42ae3de999c43c83c66";
    public static final String AUTH_TOKEN = "f049b060049b41acf3ef54f6752386c3";

    public Message sendSms(String toPhoneNumber, String smsContent) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        return Message.creator(new PhoneNumber(toPhoneNumber), new PhoneNumber("84982291607"),
                smsContent).create();
    }
}
