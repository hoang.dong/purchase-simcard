package com.jv.bank.simcard.service;

import com.jv.bank.simcard.enumeration.SupplierEnum;

/**
 * @author hoang.dong
 */
public interface VoucherService {

    /**
     * URL and content-type of third-party
     */
    String THIRD_PARTY_URL = "http://localhost:8080/phone-card/rest/third-party/voucher";
    String CONTENT_TYPE = "application/com.vn.bank.sim.card.third.party.interface+json;charset=utf-8";
    String ACCEPT_TYPE = "application/com.vn.bank.sim.card.third.party.interface+json;charset=utf-8";

    String registerVoucher(SupplierEnum supplierEnum, String kindCode);

    boolean cancelVoucher(String voucherCode);

    default String formatRegisterVoucherUrl(String supplierCode, String kindCode) {
        return String.format("%s/supplier/%s/kind/%s", THIRD_PARTY_URL, supplierCode, kindCode);
    }

    default String formatCancelVoucherUrl(String voucherCode) {
        return String.format("%s/%s", THIRD_PARTY_URL, voucherCode);
    }
}
