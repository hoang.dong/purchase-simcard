package com.jv.bank.simcard.service;

import com.jv.bank.simcard.abstracts.AbstractService;
import com.jv.bank.simcard.exception.ComponentException;
import com.jv.bank.simcard.model.CardInvoice;
import com.jv.bank.simcard.model.InvoiceRequest;

import java.math.BigInteger;

/**
 * @author hoang.dong
 */
public interface CardInvoiceService extends AbstractService<CardInvoice, BigInteger> {

    String createInvoice(InvoiceRequest dto) throws ComponentException;
}
