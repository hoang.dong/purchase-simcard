package com.jv.bank.simcard.service.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jv.bank.simcard.enumeration.SupplierEnum;
import com.jv.bank.simcard.model.restricted.ResultStatus;
import com.jv.bank.simcard.model.restricted.SingleResponse;
import com.jv.bank.simcard.service.VoucherService;
import com.jv.bank.simcard.utilities.Constants;
import com.jv.bank.simcard.utilities.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class VoucherServiceImpl implements VoucherService {

    @Override
    public String registerVoucher(SupplierEnum supplierEnum, String kindCode) {
        String result = Constants.EMPTY;
        try {
            String url = formatRegisterVoucherUrl(supplierEnum.getCode(), kindCode);
            ResponseEntity<String> response = HttpUtils.executePost(url, null, CONTENT_TYPE, ACCEPT_TYPE);
            if (HttpStatus.OK.equals(response.getStatusCode())) {
                try {
                    SingleResponse<String> singleResponse = new ObjectMapper().readValue(response.getBody(), SingleResponse.class);
                    if (singleResponse.getResultStatus() != null && singleResponse.getResultStatus().isSuccessful()) {
                        result = singleResponse.getData();
                    }
                } catch (Exception e) {
                    log.error("Cannot read value from result when register a voucher: {}", e.getMessage());
                    result = Constants.EMPTY;
                }
            }
        } finally {
            log.info("register voucher by kind({}): {}", kindCode, !StringUtils.isEmpty(result) ? "successful" : "fail");
        }
        return result;
    }

    @Override
    public boolean cancelVoucher(String voucherCode) {
        boolean result = false;
        try {
            String url = formatCancelVoucherUrl(voucherCode);
            ResponseEntity<String> response = HttpUtils.executePut(url, null, CONTENT_TYPE, ACCEPT_TYPE);
            if (HttpStatus.OK.equals(response.getStatusCode())) {
                try {
                    ResultStatus resultStatus = new ObjectMapper().readValue(response.getBody(), ResultStatus.class);
                    result = resultStatus != null && resultStatus.isSuccessful();
                } catch (Exception e) {
                    log.error("Cannot read value from result when cancel a voucher: {}", e.getMessage());
                    result = false;
                }
            }
        } finally {
            log.info("Cancel voucher({}): {}", voucherCode, result ? "successful" : "fail");
        }
        return result;
    }
}