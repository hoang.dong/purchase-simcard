package com.jv.bank.simcard.service;

import com.jv.bank.simcard.abstracts.AbstractService;
import com.jv.bank.simcard.model.BankTransactionHistory;

import java.math.BigInteger;

/**
 * @author hoang.dong
 */
public interface BankTransactionHistoryService extends AbstractService<BankTransactionHistory, BigInteger> {
}
