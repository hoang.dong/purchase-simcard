package com.jv.bank.simcard.service.thirdparty;

import com.jv.bank.simcard.service.VoucherGenerateService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class VoucherGenerateServiceImpl implements VoucherGenerateService {

    @Override
    public String registerVoucher(String supplierCode, String kindCode) {
        String voucherCode = supplierCode + kindCode + RandomStringUtils.random(10, true, true);
        log.info("register voucher: {}", voucherCode.toUpperCase());
        return voucherCode.toUpperCase();
    }

    @Override
    public boolean cancelVoucher(String voucher) {
        log.info("cancel voucher: {}", voucher);
        return true;
    }
}
