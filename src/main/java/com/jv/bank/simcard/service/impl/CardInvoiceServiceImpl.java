package com.jv.bank.simcard.service.impl;

import com.jv.bank.simcard.abstracts.AbstractServiceImpl;
import com.jv.bank.simcard.entity.CardInvoiceEntity;
import com.jv.bank.simcard.enumeration.SupplierEnum;
import com.jv.bank.simcard.enumeration.TransactionTypeEnum;
import com.jv.bank.simcard.exception.ComponentException;
import com.jv.bank.simcard.exception.ValidateException;
import com.jv.bank.simcard.mapper.CardInvoiceMapper;
import com.jv.bank.simcard.model.BankAccount;
import com.jv.bank.simcard.model.BankTransactionHistory;
import com.jv.bank.simcard.model.CardInvoice;
import com.jv.bank.simcard.model.CardKind;
import com.jv.bank.simcard.model.InvoiceRequest;
import com.jv.bank.simcard.repository.CardInvoiceRepository;
import com.jv.bank.simcard.service.BankAccountService;
import com.jv.bank.simcard.service.BankTransactionHistoryService;
import com.jv.bank.simcard.service.CardInvoiceService;
import com.jv.bank.simcard.service.CardKindService;
import com.jv.bank.simcard.service.VoucherService;
import com.jv.bank.simcard.utilities.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.regex.Pattern;

/**
 * @author hoang.dong
 */
@Slf4j
@Component
public class CardInvoiceServiceImpl extends AbstractServiceImpl<
        BigInteger,
        CardInvoiceEntity,
        CardInvoice,
        CardInvoiceMapper,
        CardInvoiceRepository
        >
        implements CardInvoiceService {

    @Autowired
    CardKindService kindService;
    @Autowired
    VoucherService voucherService;
    @Autowired
    BankAccountService accountService;
    @Autowired
    BankTransactionHistoryService transactionHistoryService;


    // @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    @Override
    public String createInvoice(InvoiceRequest invoiceRequest) throws ComponentException {

        // input validation?
        CardKind kind = validateKind(invoiceRequest);
        validatePhoneNumber(invoiceRequest.getPhoneNumber());
        validateBankingAccount(invoiceRequest, kind);

        // register a voucher
        SupplierEnum supplierEnum = SupplierEnum.detectValue(invoiceRequest.getSupplierCode());
        String voucherCode = voucherService.registerVoucher(supplierEnum, invoiceRequest.getKindCode());
        if (StringUtils.isBlank(voucherCode)) {
            String errMsg = String.format(
                    "Cannot register voucher with kind(%s) from supplier(%s)",
                    invoiceRequest.getKindCode(),
                    supplierEnum.getDescription());
            log.error(errMsg);
            throw new ComponentException(errMsg);
        }

        try {
            // create invoice
            CardInvoice invoice = mapper.toInvoiceDto(invoiceRequest);
            invoice.setPrice(kind.getPrice());
            invoice.setUserName(invoice.getBankAccountName());
            invoice.setVoucherCode(voucherCode);
            invoice.setInvoiceDate(Calendar.getInstance().getTime());
            create(invoice);
            log.info("create invoice successful");

            // update available balance for current account
            BankAccount account = accountService.getByVisaCardNumber(invoiceRequest.getVisaCardNumber());
            BigDecimal currentBalance = account.getAvailableBalances();
            BigDecimal newBalance = currentBalance.subtract(kind.getPrice());
            account.setAvailableBalances(newBalance);
            accountService.update(account);
            log.info("update account successful");

            // create transaction history
            BankTransactionHistory transactionHistory = new BankTransactionHistory();
            transactionHistory.setAccount(account);
            transactionHistory.setCurrentBalance(currentBalance);
            transactionHistory.setTransactionAmount(kind.getPrice());
            transactionHistory.setNewBalance(newBalance);
            transactionHistory.setTransactionType(TransactionTypeEnum.PHONE_CARD_RECHARGE.name());
            transactionHistory.setDescription(TransactionTypeEnum.PHONE_CARD_RECHARGE.getDescription());
            transactionHistory.setTransactionDate(Calendar.getInstance().getTime());
            transactionHistoryService.create(transactionHistory);
            log.info("create transaction history successful");

        } catch (Exception e) {
            log.error("Unexpected exception while creating invoice: {}", e.getMessage());

            // rollback register voucher when create invoice has exception
            voucherService.cancelVoucher(voucherCode);

            // throw an exception
            throw new ComponentException("Unexpected error while purchasing a prepaid voucher.");
        }
        // new SendMobileSmsService().sendSms(invoiceRequest.getPhoneNumber(), "voucher code " + voucherCode);
        return voucherCode;
    }

    /**
     * phone number is correct?
     *
     * @param phoneNumber
     * @throws ValidateException
     */
    private void validatePhoneNumber(String phoneNumber) throws ValidateException {
        if (StringUtils.isBlank(phoneNumber)) {
            String errMsg = String.format("phone number cannot be null or blank");
            log.error(errMsg);
            throw new ValidateException(errMsg);
        }
        if (!Pattern.matches(Constants.VIETNAM_PHONE_NUMBER_REGEX, phoneNumber)) {
            String errMsg = String.format("phone number (%s) is invalid");
            log.error(errMsg);
            throw new ValidateException(errMsg);
        }
    }

    /**
     * Validate select Kind of Card
     *
     * @param invoiceRequest
     * @return CardKindDto
     * @throws ComponentException
     */
    private CardKind validateKind(InvoiceRequest invoiceRequest) throws ComponentException {
        if (StringUtils.isAnyBlank(invoiceRequest.getSupplierCode(), invoiceRequest.getKindCode())) {
            LinkedList<String> errFields = new LinkedList<>();
            if (StringUtils.isBlank(invoiceRequest.getSupplierCode())) {
                errFields.add("supplierCode");
            }
            if (StringUtils.isBlank(invoiceRequest.getKindCode())) {
                errFields.add("kindCode");
            }
            String errMsg = String.format("the value of (%s) cannot be null or blank", String.join(", ", errFields));
            log.error(errMsg);
            throw new ValidateException(errMsg);
        }
        // NotFoundException will be thrown when the selected kind is not existed
        return kindService.findByIdentifier(invoiceRequest.getKindCode(), invoiceRequest.getSupplierCode());
    }

    /**
     * validate banking account
     *
     * @param invoiceRequest
     * @param kind
     * @throws ComponentException
     */
    private void validateBankingAccount(InvoiceRequest invoiceRequest, CardKind kind) throws ComponentException {

        // Blank values validation
        if (StringUtils.isAnyBlank(
                invoiceRequest.getAccountName(),
                invoiceRequest.getVisaCardNumber(),
                invoiceRequest.getAuthorisedSignature(),
                invoiceRequest.getValidPeriod(),
                invoiceRequest.getPinNumber())) {

            LinkedList<String> errFields = new LinkedList<>();
            if (StringUtils.isBlank(invoiceRequest.getAccountName())) {
                errFields.add("accountName");
            }
            if (StringUtils.isBlank(invoiceRequest.getVisaCardNumber())) {
                errFields.add("visaCardNumber");
            }
            if (StringUtils.isBlank(invoiceRequest.getAuthorisedSignature())) {
                errFields.add("authorisedSignature");
            }
            if (StringUtils.isBlank(invoiceRequest.getValidPeriod())) {
                errFields.add("validPeriod");
            }
            if (StringUtils.isBlank(invoiceRequest.getPinNumber())) {
                errFields.add("pinNumber");
            }
            String errMsg = String.format("the value of (%s) cannot be null or blank", String.join(", ", errFields));
            log.error(errMsg);
            throw new ValidateException(errMsg);
        }

        // is Banking account existed?
        // NotFoundException will be thrown when the account is not existed
        BankAccount account = accountService.getByVisaCardNumber(invoiceRequest.getVisaCardNumber());

        // is banking account parameters matched account in database?
        if (!StringUtils.equals(account.getAccountName(), invoiceRequest.getAccountName())
                || !StringUtils.equals(account.getCardValidPeriod(), invoiceRequest.getValidPeriod())
                || !StringUtils.equals(account.getAuthorisedSignature(), invoiceRequest.getAuthorisedSignature())
                || !StringUtils.equals(account.getPinNumber(), invoiceRequest.getPinNumber())) {
            throw new ValidateException("Banking account is invalid");
        }

        // valid period is correct?
        String currentPeriod = new SimpleDateFormat("yyyyMM").format(Calendar.getInstance().getTime());
        if (Integer.parseInt(currentPeriod) > Integer.parseInt(account.getCardValidPeriod())) {
            throw new ValidateException("Card has expired");
        }

        // is available balances is greater than current amount of selected voucher?
        if (account.getAvailableBalances() == null || account.getAvailableBalances().compareTo(kind.getPrice()) < 0) {
            throw new ValidateException("available balances is not enough for this transaction");
        }
    }
}
