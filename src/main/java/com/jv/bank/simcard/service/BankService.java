package com.jv.bank.simcard.service;

import com.jv.bank.simcard.abstracts.AbstractService;
import com.jv.bank.simcard.model.Bank;

/**
 * @author hoang.dong
 */
public interface BankService extends AbstractService<Bank, Integer> {
}
