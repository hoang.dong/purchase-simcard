package com.jv.bank.simcard.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author hoang.dong
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends ComponentException {

    private String objectType;
    private String fieldName;
    private Object value;

    public NotFoundException(String objectType, String fieldName, Object value) {
        super(String.format("%s not found by %s : '%s'", objectType, fieldName, value));
        this.objectType = objectType;
        this.fieldName = fieldName;
        this.value = value;
    }

    public NotFoundException(Class clazz, String fieldName, Object value) {
        super(String.format("%s not found by %s : '%s'", clazz.getName(), fieldName, value));
        this.objectType = clazz.getName();
        this.fieldName = fieldName;
        this.value = value;
    }

    public String getObjectType() {
        return objectType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getValue() {
        return value;
    }
}
