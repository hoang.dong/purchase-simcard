package com.jv.bank.simcard.exception;

import com.jv.bank.simcard.utilities.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author hoang.dong
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ComponentException extends Exception {

    private String errorCode;
    private String message;

    public ComponentException(String message) {
        super(message);
        this.errorCode = Constants.EMPTY;
        this.message = message;
    }

    public ComponentException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
