package com.jv.bank.simcard.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends ComponentException {

    private String user;
    private String permission;

    public UnauthorizedException(String user, String permission) {
        super(String.format("Login user doesn't have enough permission to access this functionality"));
        this.user = user;
        this.permission = permission;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
