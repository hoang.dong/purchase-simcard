package com.jv.bank.simcard.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author hoang.dong
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ValidateException extends ComponentException {

    public ValidateException(String message) {
        super(message);
    }

    public ValidateException(String errorCode, String message) {
        super(errorCode, message);
    }
}
