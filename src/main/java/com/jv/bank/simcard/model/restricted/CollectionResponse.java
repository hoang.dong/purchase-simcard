package com.jv.bank.simcard.model.restricted;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CollectionResponse<T> implements Serializable {

    private Collection<T> data;
    private Long total = 0L;
    private ResultStatus resultStatus;

    public CollectionResponse(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
        this.data = Collections.EMPTY_LIST;
        this.total = 0L;
    }

    public CollectionResponse(Collection<T> data, ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
        this.data = data;
        this.total = this.data != null ? this.data.size() : 0L;
    }
}
