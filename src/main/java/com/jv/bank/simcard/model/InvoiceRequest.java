package com.jv.bank.simcard.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class InvoiceRequest implements Serializable {

    private String kindCode;
    private String supplierCode;
    private String phoneNumber;
    private String accountName;
    private String visaCardNumber;
    private String validPeriod;
    private String authorisedSignature;
    private String pinNumber;
}
