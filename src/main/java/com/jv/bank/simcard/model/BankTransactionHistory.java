package com.jv.bank.simcard.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class BankTransactionHistory implements Serializable {

    private BigInteger id;
    private BankAccount account;
    private String transactionType;
    private String description;
    private BigDecimal transactionAmount;
    private BigDecimal currentBalance;
    private BigDecimal newBalance;
    private Date transactionDate;
}
