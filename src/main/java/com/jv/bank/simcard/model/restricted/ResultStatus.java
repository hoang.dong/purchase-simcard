package com.jv.bank.simcard.model.restricted;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultStatus implements Serializable {

    private boolean successful;
    private String errorCode;
    private String message;

    public ResultStatus(boolean successful) {
        this.successful = successful;
    }

    public ResultStatus(boolean successful, String message) {
        this.successful = successful;
        this.message = message;
    }
}
