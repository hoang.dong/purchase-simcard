package com.jv.bank.simcard.model.restricted;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SingleResponse<T> implements Serializable {

    private T data;
    private ResultStatus resultStatus;

    public SingleResponse(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }
}
