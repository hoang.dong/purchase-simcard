package com.jv.bank.simcard.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class BankAccount implements Serializable {

    private Integer id;
    private Bank bank;
    private String accountName;
    private String phoneNumber;
    private String email;
    private String visaCardNumber;
    private String pinNumber;
    private String cardValidPeriod;
    private String authorisedSignature;
    private BigDecimal availableBalances;
    private Date creationDate;
}
