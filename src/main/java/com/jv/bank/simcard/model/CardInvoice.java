package com.jv.bank.simcard.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CardInvoice implements Serializable {

    private Integer id;
    private String voucherCode;

    private String kindCode;
    private String supplierCode;
    private BigDecimal price;

    private String userName;
    private String phoneNumber;

    private String bankAccountName;
    private String bankAccountNumber;
    private String bankValidPeriod;
    private String bankAuthorisedSignature;

    private Date invoiceDate;
}
