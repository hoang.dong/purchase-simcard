package com.jv.bank.simcard.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CardKind implements Serializable {

    private String code;
    private CardSupplier supplier;
    private String description;
    private BigDecimal price;
}
