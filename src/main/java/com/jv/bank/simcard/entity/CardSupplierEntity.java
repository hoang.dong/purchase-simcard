package com.jv.bank.simcard.entity;

import com.jv.bank.simcard.abstracts.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * network companies
 *
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "card_supplier")
public class CardSupplierEntity extends AbstractEntity<Integer> {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    public CardSupplierEntity(Integer id) {
        this.id = id;
    }
}
