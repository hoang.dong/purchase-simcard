package com.jv.bank.simcard.entity;

import com.jv.bank.simcard.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "bank_transaction_history")
public class BankTransactionHistoryEntity extends AbstractEntity<BigInteger> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private BigInteger id;

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private BankAccountEntity account;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "description")
    private String description;

    @Column(name = "transaction_amount")
    private BigDecimal transactionAmount;

    @Column(name = "current_balance")
    private BigDecimal currentBalance;

    @Column(name = "new_balance")
    private BigDecimal newBalance;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transaction_date")
    private Date transactionDate;
}
