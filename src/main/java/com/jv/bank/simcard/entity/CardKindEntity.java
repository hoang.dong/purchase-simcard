package com.jv.bank.simcard.entity;

import com.jv.bank.simcard.abstracts.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "card_kind")
public class CardKindEntity extends AbstractEntity<CardKindPK> {

    @EmbeddedId
    private CardKindPK id;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private BigDecimal price;
}
