package com.jv.bank.simcard.entity;

import com.jv.bank.simcard.abstracts.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "bank_account")
public class BankAccountEntity extends AbstractEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bank_id", referencedColumnName = "id")
    private BankEntity bank;

    @Column(name = "account_name")
    private String accountName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "visa_card_number")
    private String visaCardNumber;

    @Column(name = "pin_number")
    private String pinNumber;

    @Column(name = "card_valid_period")
    private String cardValidPeriod;

    @Column(name = "authorised_signature")
    private String authorisedSignature;

    @Column(name = "available_balances")
    private BigDecimal availableBalances;

    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date")
    private Date creationDate;
}
