package com.jv.bank.simcard.entity;

import com.jv.bank.simcard.abstracts.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author hoang.dong
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "bank")
public class BankEntity extends AbstractEntity<Integer> {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    public BankEntity(Integer id) {
        this.id = id;
    }
}
