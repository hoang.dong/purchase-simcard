package com.jv.bank.simcard;

import com.jv.bank.simcard.entity.BankAccountEntity;
import com.jv.bank.simcard.entity.BankEntity;
import com.jv.bank.simcard.entity.CardKindEntity;
import com.jv.bank.simcard.entity.CardKindPK;
import com.jv.bank.simcard.entity.CardSupplierEntity;
import com.jv.bank.simcard.enumeration.KindCodeEnum;
import com.jv.bank.simcard.enumeration.SupplierEnum;
import com.jv.bank.simcard.repository.BankAccountRepository;
import com.jv.bank.simcard.repository.BankRepository;
import com.jv.bank.simcard.repository.CardKindRepository;
import com.jv.bank.simcard.repository.CardSupplierRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Arrays;
import java.util.Calendar;

@SpringBootApplication
@EnableJpaAuditing
@EntityScan(basePackages = {"com.jv.bank.simcard.entity"})
@EnableJpaRepositories
@Configuration
public class PurchaseSimCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(PurchaseSimCardApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(Duration.ofMillis(3000))
                .setReadTimeout(Duration.ofMillis(3000))
                .build();
    }

    /**
     * Setup init data for testing
     */
    @Autowired
    CardSupplierRepository supplierRepository;
    @Autowired
    CardKindRepository kindRepository;
    @Autowired
    BankRepository bankRepository;
    @Autowired
    BankAccountRepository accountRepository;

    @Bean
    InitializingBean sendDatabase() {
        return () -> {
            Arrays.stream(SupplierEnum.values()).forEach(supplier -> {
                if (!SupplierEnum.UNKN.equals(supplier)) {
                    createSupplier(supplier);
                    createKinds(supplier);
                }
            });
            createBank();
            createAccount();
        };
    }

    private void createSupplier(SupplierEnum supplier) {
        CardSupplierEntity entity = new CardSupplierEntity();
        entity.setId(supplier.getId());
        entity.setCode(supplier.name());
        entity.setName(supplier.getDescription());
        supplierRepository.save(entity);
    }

    private void createKinds(SupplierEnum supplier) {
        Arrays.stream(KindCodeEnum.values()).forEach(kind -> {
            CardKindEntity entity = new CardKindEntity();
            entity.setId(new CardKindPK(kind.getCode(), new CardSupplierEntity(supplier.getId())));
            entity.setDescription(kind.getDescription());
            entity.setPrice(kind.getAmount());
            kindRepository.save(entity);
        });
    }

    private void createBank() {
        BankEntity entity = new BankEntity();
        entity.setId(1);
        entity.setCode("NAB");
        entity.setName("National Australia Bank");
        entity.setAddress("Australia");
        bankRepository.save(entity);
    }

    private void createAccount() {
        BankAccountEntity entity = new BankAccountEntity();
        entity.setBank(new BankEntity(1));
        entity.setAccountName("hoang.dong");
        entity.setVisaCardNumber("0123456789");
        entity.setPinNumber("123456");
        entity.setAuthorisedSignature("111");
        entity.setAvailableBalances(BigDecimal.valueOf(10000000));
        entity.setCardValidPeriod("202512");
        entity.setEmail("davidtroc0903@gmail.com");
        entity.setPhoneNumber("0982291607");
        entity.setCreationDate(Calendar.getInstance().getTime());
        accountRepository.save(entity);
    }
}