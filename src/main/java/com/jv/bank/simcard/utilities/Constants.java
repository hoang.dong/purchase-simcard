package com.jv.bank.simcard.utilities;

/**
 * @author hoang.dong
 */
public class Constants {

    private Constants() {
    }

    public static final String EMPTY = "";

    public static final String VIETNAM_PHONE_NUMBER_REGEX = "(\\+84|84|0)+(9|1[0-9])+([0-9]{8})\\b";
}
