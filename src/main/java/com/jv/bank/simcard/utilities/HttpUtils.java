package com.jv.bank.simcard.utilities;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author hoang.dong
 */
public class HttpUtils {

    public static ResponseEntity<String> executeGet(String url, String contentType, String acceptType) {
        HttpHeaders headers = initiateHeader(contentType, acceptType);
        HttpEntity request = new HttpEntity<>(headers);
        return new RestTemplate().postForEntity(url, request, String.class);
    }

    public static ResponseEntity<String> executePost(String url, Object objectBody, String contentType, String acceptType) {
        HttpHeaders headers = initiateHeader(contentType, acceptType);
        HttpEntity request = objectBody != null ? new HttpEntity<>(objectBody, headers) : new HttpEntity<>(headers);
        return new RestTemplate().postForEntity(url, request, String.class);
    }

    public static ResponseEntity<String> executePut(String url, Object objectBody, String contentType, String acceptType) {
        HttpHeaders headers = initiateHeader(contentType, acceptType);
        HttpEntity request = objectBody != null ? new HttpEntity<>(objectBody, headers) : new HttpEntity<>(headers);
        return new RestTemplate().exchange(url, HttpMethod.PUT, request, String.class);
    }

    private static HttpHeaders initiateHeader(String contentType, String acceptType) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", contentType);
        headers.add("Accept", acceptType);
        return headers;
    }
}
