package com.jv.bank.simcard.enumeration;

import java.math.BigDecimal;

/**
 * @author hoang.dong
 */
public enum KindCodeEnum {

    _0010("0010", BigDecimal.valueOf(10000), "10,000 VND"),
    _0020("0020", BigDecimal.valueOf(20000), "20,000 VND"),
    _0050("0050", BigDecimal.valueOf(50000), "50,000 VND"),
    _0100("0100", BigDecimal.valueOf(100000), "100,000 VND"),
    _0200("0200", BigDecimal.valueOf(200000), "200,000 VND"),
    _0500("0500", BigDecimal.valueOf(500000), "500,000 VND");

    private final String code;
    private final BigDecimal amount;
    private final String description;

    KindCodeEnum(String code, BigDecimal amount, String description) {
        this.code = code;
        this.amount = amount;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }
}
