package com.jv.bank.simcard.enumeration;

import java.util.Arrays;

/**
 * @author hoang.dong
 */
public enum SupplierEnum {

    VTEL(1, "VTEL", "Viettel"),
    MOBI(2, "MOBI", "Mobiphone"),
    VNMB(3, "VNMB", "Vietnamobile"),
    UNKN(9, "UNKN", "Unknown");

    private final int id;
    private final String code;
    private final String description;

    SupplierEnum(int id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }

    /**
     * detect supplier enum by code
     *
     * @param code
     * @return SupplierEnum
     */
    public static SupplierEnum detectValue(String code) {
        return Arrays.stream(values()).filter(en -> en.name().equalsIgnoreCase(code)).findFirst().orElse(UNKN);
    }

    public int getId() {
        return this.id;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return this.description;
    }
}
