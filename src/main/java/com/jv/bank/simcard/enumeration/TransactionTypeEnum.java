package com.jv.bank.simcard.enumeration;

/**
 * @author hoang.dong
 */
public enum TransactionTypeEnum {

    WITHDRAWAL("withdrawal"),
    TRANSFERS("transfers"),
    PHONE_CARD_RECHARGE("phone card recharge"),
    BILL_PAYMENT("bill payment");

    private final String description;

    TransactionTypeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
