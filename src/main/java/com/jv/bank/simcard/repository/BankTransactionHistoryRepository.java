package com.jv.bank.simcard.repository;

import com.jv.bank.simcard.entity.BankTransactionHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

/**
 * @author hoang.dong
 */
@Repository
public interface BankTransactionHistoryRepository extends JpaRepository<BankTransactionHistoryEntity, BigInteger> {
}
