package com.jv.bank.simcard.repository;

import com.jv.bank.simcard.entity.BankAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

/**
 * @author hoang.dong
 */
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccountEntity, Integer> {

    BankAccountEntity getByVisaCardNumber(String visaCardNumber);
}
