package com.jv.bank.simcard.repository;

import com.jv.bank.simcard.entity.CardSupplierEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author hoang.dong
 */
@Repository
public interface CardSupplierRepository extends JpaRepository<CardSupplierEntity, Integer> {

    CardSupplierEntity getByCode(String code);
}
