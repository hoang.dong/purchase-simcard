package com.jv.bank.simcard.repository;

import com.jv.bank.simcard.entity.CardKindEntity;
import com.jv.bank.simcard.entity.CardKindPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author hoang.dong
 */
@Repository
public interface CardKindRepository extends JpaRepository<CardKindEntity, CardKindPK> {

    List<CardKindEntity> getByIdSupplierCode(String supplierCode);
}
