package com.jv.bank.simcard.repository;

import com.jv.bank.simcard.entity.BankEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author hoang.dong
 */
@Repository
public interface BankRepository extends JpaRepository<BankEntity, Integer> {
}
