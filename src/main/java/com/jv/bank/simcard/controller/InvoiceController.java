package com.jv.bank.simcard.controller;

import com.jv.bank.simcard.exception.ComponentException;
import com.jv.bank.simcard.model.InvoiceRequest;
import com.jv.bank.simcard.model.restricted.ResultStatus;
import com.jv.bank.simcard.model.restricted.SingleResponse;
import com.jv.bank.simcard.service.CardInvoiceService;
import com.jv.bank.simcard.utilities.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for manage suppliers
 *
 * @author hoang.dong
 */
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    CardInvoiceService invoiceService;

    @PostMapping(
            consumes = "application/com.vn.bank.sim.card.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.interface+json;charset=utf-8"
    )
    @ResponseBody
    public SingleResponse createInvoice(@RequestBody InvoiceRequest invoiceRequest) throws ComponentException {
        try {
            String voucherCode = invoiceService.createInvoice(invoiceRequest);
            return new SingleResponse(voucherCode, new ResultStatus(true));
        } catch (ComponentException e) {
            return new SingleResponse(Constants.EMPTY, new ResultStatus(false, e.getMessage()));
        }
    }
}
