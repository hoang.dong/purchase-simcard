package com.jv.bank.simcard.controller;

import com.jv.bank.simcard.exception.NotFoundException;
import com.jv.bank.simcard.model.restricted.CollectionResponse;
import com.jv.bank.simcard.model.restricted.ResultStatus;
import com.jv.bank.simcard.model.restricted.SingleResponse;
import com.jv.bank.simcard.service.CardSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for manage suppliers
 *
 * @author hoang.dong
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    CardSupplierService supplierService;

    @GetMapping(
            path = "{code}",
            consumes = "application/com.vn.bank.sim.card.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.interface+json;charset=utf-8"
    )
    public SingleResponse getBySupplier(@PathVariable(name = "code") String supplierCode) throws NotFoundException {
        return new SingleResponse(supplierService.getByCode(supplierCode), new ResultStatus(true));
    }

    @GetMapping(
            path = "all",
            consumes = "application/com.vn.bank.sim.card.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.interface+json;charset=utf-8"
    )
    public CollectionResponse getAllSuppliers() {
        return new CollectionResponse(supplierService.findAll(), new ResultStatus(true));
    }
}
