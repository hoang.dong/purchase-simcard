package com.jv.bank.simcard.controller.thirdparty;

import com.jv.bank.simcard.model.restricted.ResultStatus;
import com.jv.bank.simcard.model.restricted.SingleResponse;
import com.jv.bank.simcard.service.VoucherGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hoang.dong
 */
@RestController
@RequestMapping("/third-party/voucher")
public class VoucherThirdpartyController {

    @Autowired
    VoucherGenerateService voucherGenerateService;

    @PostMapping(path = "/supplier/{supplierCode}/kind/{kindCode}",
            consumes = "application/com.vn.bank.sim.card.third.party.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.third.party.interface+json;charset=utf-8"
    )
    public SingleResponse registerVoucher(@PathVariable("supplierCode") String supplierCode,
                                          @PathVariable("kindCode") String kindCode) {
        String voucherCode = voucherGenerateService.registerVoucher(supplierCode, kindCode);
        return new SingleResponse(voucherCode, new ResultStatus(true));
    }

    @PutMapping(path = "{voucherCode}",
            consumes = "application/com.vn.bank.sim.card.third.party.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.third.party.interface+json;charset=utf-8"
    )
    public ResultStatus cancelVoucher(@PathVariable("voucherCode") String voucherCode) {
        return new ResultStatus(voucherGenerateService.cancelVoucher(voucherCode));
    }
}
