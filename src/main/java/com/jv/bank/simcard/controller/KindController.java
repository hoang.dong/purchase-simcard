package com.jv.bank.simcard.controller;

import com.jv.bank.simcard.exception.ComponentException;
import com.jv.bank.simcard.model.restricted.CollectionResponse;
import com.jv.bank.simcard.model.restricted.ResultStatus;
import com.jv.bank.simcard.service.CardKindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * To show all card kind from c
 *
 * @author hoang.dong
 */
@RestController
@RequestMapping("/kind")
public class KindController {

    @Autowired
    private CardKindService kindService;

    @GetMapping(
            path = "supplier/{code}",
            consumes = "application/com.vn.bank.sim.card.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.interface+json;charset=utf-8"
    )
    public CollectionResponse getBySupplier(@PathVariable(name = "code") String supplierCode) throws ComponentException {
        return new CollectionResponse(kindService.getBySupplierCode(supplierCode), new ResultStatus(true));
    }

    @GetMapping(
            path = "all",
            consumes = "application/com.vn.bank.sim.card.interface+json;charset=utf-8",
            produces = "application/com.vn.bank.sim.card.interface+json;charset=utf-8")
    public CollectionResponse getAllKinds() {
        return new CollectionResponse(kindService.findAll(), new ResultStatus(true));
    }
}
